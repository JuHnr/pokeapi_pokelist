// src/types/dataTypes.ts
export interface Pokemon {
    name: string;
    url: string;
}

export interface PokemonData {
    results: Pokemon[];
}

export interface Sprite {
    back_default: string | null;
    back_female: string | null;
    back_shiny: string | null;
    back_shiny_female: string | null;
    front_default: string | null;
    front_female: string | null;
    front_shiny: string | null;
    front_shiny_female: string | null;
    other?: {
        dream_world?: {
            front_default: string | null;
            front_female: string | null;
        };
        'official-artwork'?: {
            front_default: string;
        };
    };
};

export interface PokemonUrlData {
    height: number;
    name: string;
    sprites: Sprite;
    weight: number;
};

export interface CardProps {
    name: string;
    height: number;
    weight: number;
    img?: string;
}