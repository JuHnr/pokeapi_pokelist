import "./../styles/Header.css";

function Header() {

    return (
        <header className="Header">
            <h1 className="Header-title">PokeList</h1>
        </header>

    );
}

export default Header