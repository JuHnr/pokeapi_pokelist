import { CardProps } from "../types/dataTypes";
import "./../styles/Card.css"

function Card(props: CardProps) {

  const { name, height, weight, img } = props;

  const formattedName = (name).charAt(0).toLocaleUpperCase() + (name).substring(1);
  const heightInMeters = (height * 0.1).toFixed(1);
  const weightInKg = (weight * 0.1).toFixed(1);

  return (
    <div className="Cards-container">
      <div className="Cards-img-container">
        <img src={img} alt={name} />
      </div>

      <div className="Cards-infos">
        <h1>{formattedName}</h1>
        <div className="Cards-infos-grid">
          <p className="Cards-infos-grid-title">Height</p>
          <p className="Cards-infos-grid-value">{heightInMeters} <span className="value-unit">m</span></p>
          <p className="Cards-infos-grid-title">Weight</p>
          <p className="Cards-infos-grid-value">{weightInKg} <span className="value-unit">kg</span></p>
        </div>
      </div>

    </div>
  );
}

export default Card