import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Details from './pages/Details.tsx';
import Homepage from './pages/Homepage.tsx';
import data from "./data.json";
import { PokemonData } from "./assets/types/dataTypes";

const pokemonData: PokemonData = data;

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <Homepage results={pokemonData.results} />,
      },
      {
        path: "/pokemons/:pokemonId",
        element: <Details />,
      },
    ]
  }

]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
