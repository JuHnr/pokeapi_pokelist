import './App.css'
import Header from './assets/components/Header';
import { Outlet } from 'react-router-dom';
import Footer from './assets/components/Footer';

function App() {

  return (
    <>
      <Header />
      <Outlet />
      <Footer />
    </>
  );
}

export default App
