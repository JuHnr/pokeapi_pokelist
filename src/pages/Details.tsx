import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import { PokemonUrlData } from "../assets/types/dataTypes";
import Card from "../assets/components/Card";
import "./../assets/styles/Details.css"

function Details() {

  const { pokemonId } = useParams();

  const [pokemon, setPokemon] = useState<PokemonUrlData | null>(null);

  useEffect(() => {
    const API_URL = `https://pokeapi.co/api/v2/pokemon/${pokemonId}`;
    axios.get<PokemonUrlData>(API_URL)
      .then((response) => {
        setPokemon(response.data);
      })
  }, []);

 
  if (pokemon === null) {
    return <p>Loading...</p>;
  }

  const pokemonImg = pokemon.sprites.other?.["official-artwork"]?.front_default;

  return (
    <div className="Details">
      <Link to="/" className="Details-back-link"> ← Back to the list </Link>
      <Card name={pokemon.name} height={pokemon.height} weight={pokemon.weight} img={pokemonImg} />
    </div>
  );
}

export default Details