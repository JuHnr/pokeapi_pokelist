import { PokemonData, Pokemon } from './../assets/types/dataTypes';
import { Link } from "react-router-dom";
import { useState } from "react";
import SearchIcon from "./../assets/search-icon.svg";
import "./../assets/styles/Homepage.css";


function Homepage(props: PokemonData) {

    const { results } = props;

    const [searchValue, setSearchValue] = useState("");
    const [searchResults, setSearchResults] = useState<Pokemon[]>(results);


    // Handle input value change
    function handleSearchChange(event: React.ChangeEvent<HTMLInputElement>) {
        setSearchValue(event.target.value)

        // if input is empty, display all pokemons
        if (event.target.value === "") {
            setSearchResults(results);
        }
    }

    // Handle click on the search button
    function handleClick() {
        const filteredResults = results.filter((pokemon: Pokemon) =>
            pokemon.name.toLowerCase().includes(searchValue.toLowerCase())
        );
        setSearchResults(filteredResults);
    };

    return (
        <div className='Homepage'>
            <div className='Homepage-searchBar-container border'>
                <h2>Find a pokemon :</h2>
                <div className="Homepage-searchBar">
                    <input
                        type="search"
                        name="searchBar"
                        id="searchBar"
                        className="Homepage-searchBar-input"
                        onChange={handleSearchChange}
                        value={searchValue}
                        placeholder="Search..."
                    />
                    <button type="submit" className="Homepage-searchBar-button" onClick={handleClick}>
                        <img src={SearchIcon} alt="search icon" />
                    </button>
                </div>
            </div>


            <div className="Homepage-search-results border">
                <h2>Pokemon list : </h2>

                {searchResults.length > 0 ? (
                    <ul className="Homepage-search-results-list">
                        {searchResults.map((pokemon: Pokemon) => (
                            <li key={pokemon.name} className="Homepage-search-results-list-item">
                                <Link to={`/pokemons/${pokemon.name}`}>
                                    {pokemon.name.charAt(0).toLocaleUpperCase() + pokemon.name.substring(1)}
                                </Link>
                            </li>
                        ))}
                    </ul>
                ) : (
                    <p>There are no Pokemon matching your search.</p>
                )}
            </div>
            
        </div>
    );
}

export default Homepage